#ifndef MALLOC_STRUCT_H
# define MALLOC_STRUCT_H

# include <stdlib.h>
# include <pthread.h>

# define INTERNAL_SIZE_T size_t
# define NFASTBINS 10
# define NBINS 128
# define BINMAPSIZE 4

typedef struct	malloc_chunk {
  INTERNAL_SIZE_T  prev_size; /* Size of previous chunk (if free). */
  INTERNAL_SIZE_T      size;  /* Size in bytes, including overhead. */

  struct malloc_chunk* fd; /* double links -- used only if free. */
  struct malloc_chunk* bk;

  /* Only used for large blocks: pointer to next larger size.  */
  struct malloc_chunk* fd_nextsize;
  struct malloc_chunk* bk_nextsize;
}				t_mchunk;

typedef struct malloc_chunk *mfastbinptr;
typedef struct malloc_chunk *mchunkptr;

typedef struct	malloc_state
{
  /* Serialize access.  */
  int mutex;

  /* Flags (formerly in max_fast).  */
  int flags;

  /* Fastbins */
  mfastbinptr fastbinsY[NFASTBINS];

  /* Base of the topmost chunk -- not otherwise kept in a bin */
  mchunkptr top;

  /* The remainder from the most recent split of a small request */
  mchunkptr last_remainder;

  /* Normal bins packed as described above */
  mchunkptr bins[NBINS * 2 - 2];

  /* Bitmap of bins */
  unsigned int binmap[BINMAPSIZE];

  /* Linked list */
  struct malloc_state *next;

  /* Linked list for free arenas.  */
  struct malloc_state *next_free;

  /* Memory allocated from the system in this arena.  */
  INTERNAL_SIZE_T system_mem;
  INTERNAL_SIZE_T max_system_mem;
}				t_mstate;

#endif
