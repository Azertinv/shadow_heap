#include "sheap.h"

void	__sheap_debug()
{
	__sheap_debug_maps();
	__sheap_debug_save_state();
	__sheap_debug_heap_state();
}

void	__sheap_debug_chunk(t_mchunk *chunk)
{
	fprintf(stderr, "------------------------------\n");
	fprintf(stderr, " address:        %p\n", chunk);
	fprintf(stderr, " prev size:      0x%zx\n", chunk->prev_size);
	fprintf(stderr, " size:           0x%zx\n", chunk->size);
	fprintf(stderr, " fd:             %p\n", chunk->fd);
	fprintf(stderr, " bk:             %p\n", chunk->bk);
	fprintf(stderr, " real size:      0x%zx\n", REAL_SIZE(chunk));
	fprintf(stderr, " prev in use:    %s\n",
			IS_PREV_INUSE(chunk) ? "YES" : "NO");
	fprintf(stderr, " mmaped:         %s\n",
			IS_MMAPPED(chunk) ? "YES" : "NO");
	fprintf(stderr, " main arena:     %s\n",
			IS_NON_MAIN_ARENA(chunk) ? "NO" : "YES");
	fprintf(stderr, " next            %p\n", NEXT(chunk));
	fprintf(stderr, "------------------------------\n");
}

void	__sheap_debug_maps()
{
	char	buff[1024];
	ssize_t	s;
	int		fd;

	fd = open("/proc/self/maps", O_RDONLY);
	while ((s = read(fd, buff, sizeof(buff))) > 0)
		write(2, buff, (size_t)s);
	write(2, buff, (size_t)s);
	close(fd);
}

void	__sheap_debug_heap_state()
{
	t_mchunk	*i;
	size_t		j;

	// chunks
	fprintf(stderr, "heap start: %p\n", sh.heap_start);
	if (sh.heap_start == NULL)
		return ;
	i = sh.heap_start;
	while (i != sh.main_arena->top
			&& i != sh.main_arena->last_remainder
			&& i->size != 0)
	{
		__sheap_debug_chunk(i);
		i = NEXT(i);
	}
	__sheap_debug_chunk(i);
	// fastbins
	for (j = 0; j < NFASTBINS; j++)
	{
		fprintf(stderr, "fastbinsY[%zu]: ", j);
		i = sh.main_arena->fastbinsY[j];
		fprintf(stderr, "%p", i);
		while (i)
		{
			i = i->fd;
			fprintf(stderr, " -> %p", i);
		}
		fprintf(stderr, "\n");
	}
}

void	__sheap_debug_save_state()
{
	size_t		j;
	size_t		k;

	// chunks
	j = 0;
	fprintf(stderr, "------------------------------\n");
	fprintf(stderr, "saved chunks nb: %zu\n", sh.schunks_nb);
	if (sh.schunks_nb)
		fprintf(stderr, "------------------------------\n");
	while (j < sh.schunks_nb)
	{
		fprintf(stderr, " address:        %p\n", sh.schunks[j].chunk);
		fprintf(stderr, " size:           0x%zx\n", sh.schunks[j].size);
		fprintf(stderr, "------------------------------\n");
		j++;
	}
	// fastbins
	for (j = 0; j < NFASTBINS; j++)
	{
		fprintf(stderr, "fastbinsY[%zu]: ", j);
		k = 0;
		fprintf(stderr, "%p", sh.sfastbins[j][k]);
		while (sh.sfastbins[j][k])
		{
			k++;
			fprintf(stderr, " -> %p", sh.sfastbins[j][k]);
		}
		fprintf(stderr, "\n");
	}
}
