#include "sheap.h"

void	*malloc(size_t size)
{
	void	*ptr;

	if (!sh.main_arena)
		__sheap_init();
	DEBUG("malloc(size = %zu) -> ", size);
	__sheap_check_integrity();
	ptr = __libc_malloc(size);
	DEBUG("%p\n", ptr);
	__sheap_save_state();
	return ptr;
}

void	free(void *ptr)
{
	DEBUG("free(ptr = %p)\n", ptr);
	__sheap_check_integrity();
	__libc_free(ptr);
	__sheap_save_state();
}

void	*realloc(void *ptr, size_t size)
{
	void			*nptr;

	if (!sh.main_arena)
		__sheap_init();
	DEBUG("realloc(ptr = %p, size = %zu) -> ", ptr, size);
	__sheap_check_integrity();
	nptr = __libc_realloc(ptr, size);
	DEBUG("%p\n", nptr);
	__sheap_save_state();
	return nptr;
}

void	*calloc(size_t nmemb, size_t size)
{
	void			*ptr;

	if (!sh.main_arena)
		__sheap_init();
	DEBUG("calloc(nmemb = %zu, size = %zu) -> ", nmemb, size);
	__sheap_check_integrity();
	ptr = __libc_calloc(nmemb, size);
	DEBUG("%p\n", ptr);
	__sheap_save_state();
	return ptr;
}
