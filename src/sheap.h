#ifndef SHEAP_H
# define SHEAP_H

# define _GNU_SOURCE
# include <dlfcn.h>
# include <stddef.h>
# include <stdio.h>
# include <string.h>
# include <stdlib.h>
# include <malloc.h>
# include <mcheck.h>
# include <fcntl.h>
# include <unistd.h>
# include <signal.h>

# define INTEGRITY_OK 0
# define INTEGRITY_FAIL 1

# define CHUNK_ADDR(ptr) ((void*)(ptr)-(void*)&((t_mchunk*)0)->fd)
# define REAL_SIZE(chunk) ((chunk)->size & ~0x7)
# define NEXT(chunk) ((void*)(chunk) + REAL_SIZE(chunk))
# define IS_PREV_INUSE(chunk) ((chunk)->size & 0x1)
# define IS_MMAPPED(chunk) ((chunk)->size & 0x2)
# define IS_NON_MAIN_ARENA(chunk) ((chunk)->size & 0x4)

# define ENV_CRASH "SHEAP_CRASH"
# define ENV_DEBUG "SHEAP_DEBUG"
# define DEBUG(...) if (sh.debug) {fprintf(stderr, __VA_ARGS__);}

/*
 * SHeap stand for Shadow Heap
 */

# include "malloc_struct.h"

extern void *__libc_calloc(size_t nmemb, size_t size);
extern void *__libc_malloc(size_t size);
extern void __libc_free(void *ptr);
extern void *__libc_realloc(void *ptr, size_t size);

typedef struct	shadow_chunk {
	t_mchunk	*chunk;
	size_t		size;
}				t_schunk;

typedef struct	s_shadow_heap
{
	int			debug;
	int			crash;
	t_mchunk	*heap_start;
	t_mstate	*main_arena;
	size_t		schunks_nb;
	t_schunk	schunks[2048]; //TODO temp alloc might use mmap
	t_mchunk	*sfastbins[NFASTBINS][128];
}				t_shadow_heap;

extern t_shadow_heap	sh;

void	__sheap_debug();
void	__sheap_debug_maps();
void	__sheap_debug_chunk();
void	__sheap_debug_save_state();
void	__sheap_debug_heap_state();

void __attribute__((constructor)) __sheap_init(void);
int		__sheap_check_integrity(void);
void	__sheap_save_state(void);

#endif
