#include "sheap.h"

t_shadow_heap	sh;

static t_mstate	*get_main_arena_addr(void)
{
	t_mstate	*ret;
	size_t		*ptr0;
	size_t		*ptr1;

	ptr0 = __libc_malloc(0x100);
	ptr1 = __libc_malloc(0x20);
	__libc_free(ptr0);
	ret = (t_mstate*)(ptr0[0] - 88);
	ptr1 = __libc_realloc(ptr1, 0xa0);
	__libc_free(ptr1);
	return (ret);
}

void __attribute__((constructor)) __sheap_init()
{
	if (sh.main_arena)
		return ;
	bzero(&sh, sizeof(sh));
	sh.debug = getenv(ENV_DEBUG) ? 1 : 0;
	sh.crash = getenv(ENV_CRASH) ? 1 : 0;
	sh.main_arena = get_main_arena_addr();
	sh.heap_start = sh.main_arena->top;
	__sheap_save_state();
	fprintf(stderr, "SHEAP INITIALIZED\n");
}

static int	is_chunk_sane(t_mchunk *m, t_schunk *s)
{
	if (m != s->chunk)
		return (INTEGRITY_FAIL);
	if (m->size != s->size)
		return (INTEGRITY_FAIL);
	return (INTEGRITY_OK);
}

int		__sheap_integrity_fail(char *msg)
{
	fprintf(stderr, "%s\n", msg);
	__sheap_debug();
	if (sh.crash)
		kill(0, 9);
	return (INTEGRITY_FAIL);
}

int		__sheap_check_integrity()
{
	t_mchunk	*i;
	size_t		j;
	size_t		k;

	if (sh.heap_start == NULL)
		return (INTEGRITY_OK);
	// Check chunks
	i = sh.heap_start;
	j = 0;
	while (j < sh.schunks_nb)
	{
		if (is_chunk_sane(i, sh.schunks + j) != INTEGRITY_OK)
			return (__sheap_integrity_fail("Heap Corruption detected"));
		i = NEXT(i);
		j++;
	}
	// Check fastbins
	for (j = 0; j < NFASTBINS; j++)
	{
		i = sh.main_arena->fastbinsY[j];
		k = 0;
		while (i && k < 128 - 1)
		{
			if (i != sh.sfastbins[j][k])
				return (__sheap_integrity_fail("Fastbin Corruption detected"));
			i = i->fd;
			k++;
		}
		if (i != sh.sfastbins[j][k])
			return (__sheap_integrity_fail("Fastbin Corruption detected"));
	}
	return (INTEGRITY_OK);
}

void	__sheap_save_chunk_state(t_mchunk *chunk)
{
	sh.schunks[sh.schunks_nb].chunk = chunk;
	sh.schunks[sh.schunks_nb].size = chunk->size;
	sh.schunks_nb++;
}

void	__sheap_save_state()
{
	t_mchunk	*i;
	size_t		j;
	size_t		k;

	if (sh.heap_start == NULL)
		return ;
	// Save chunks
	sh.schunks_nb = 0;
	i = sh.heap_start;
	while (i != sh.main_arena->top
			&& i != sh.main_arena->last_remainder
			&& i->size != 0)
	{
		__sheap_save_chunk_state(i);
		i = NEXT(i);
	}
	__sheap_save_chunk_state(i);
	// Save fastbins
	for (j = 0; j < NFASTBINS; j++)
	{
		i = sh.main_arena->fastbinsY[j];
		k = 0;
		while (i && k < 128 - 1)
		{
			sh.sfastbins[j][k] = i;
			i = i->fd;
			k++;
		}
		sh.sfastbins[j][k] = i;
	}
}
