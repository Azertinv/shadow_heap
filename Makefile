CC=gcc
CFLAG=-Wall -Wextra -Werror -fPIC -O3 -g
LFLAG=-shared -O3 -g
NAME=sheap.so

SRC_DIR=src
OBJ_DIR=build

SRC=$(addprefix $(SRC_DIR)/, \
	 sheap.c hook.c debug.c)
OBJ=$(SRC:$(SRC_DIR)/%.c=$(OBJ_DIR)/%.o)
INC=$(addprefix $(SRC_DIR)/, \
	sheap.h malloc_struct.h)

all: $(NAME)

$(NAME): $(OBJ)
	$(CC) $(LFLAG) $^ -o $(NAME)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c $(INC)
	$(CC) $(CFLAG) -c $< -o $@

clean:
	rm -f $(OBJ)

fclean: clean
	rm -f $(NAME)

.PHONY: all clean fclean re

re: fclean all
